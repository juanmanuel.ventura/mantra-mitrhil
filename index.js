/**
 * Created by matteodellea on 14/04/15.
 */

"use strict";

var m = require("mithril");

m.layout = function (a) { return a };
m._render = m.render;
m.render = function (root, cell, forceRecreation) {
  cell = cell && cell.layout ? cell.layout(cell) : m.layout(cell);
  m._render(root, cell, forceRecreation);
};

module.exports = m;