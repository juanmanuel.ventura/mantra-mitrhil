## Description: ##

it's simply mitrhil.js patched with layout logic

## How to use: ##

**global layout:**

```
#!javascript

m.layout = function(view) {
  return [
    m('header', 'I am the header'),
    view,
    m('footer', 'I am the footer')
  ]
};
```

**page dedicated layout:**


```
#!javascript

var view = function(ctrl) { 
  return m('main','I am the content of the view');
};

var layout = function(view, ctrl) {
 return [
    m('header', 'I am the header'),
    view,
    m('footer', 'I am the footer')
 ];
};

// this is the function that has to be passed to the m.module or m.route
var finalViewWithLayout = function(ctrl){
   var v = view(ctrl);
   // app.vm is assumed to be the global view model of the app
   // in the bind the context can also be null
   v.layout = layout.bind(app.vm, v, ctrl);
   return v;
};

```